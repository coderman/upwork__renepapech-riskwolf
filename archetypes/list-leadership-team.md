---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
social:
  linkedin:
    url:
headless: true
---

