---
title: "Thomas Krapf"
date: 2021-01-01T17:57:24+01:00
draft: false
social:
 linkedin:
  url: 'https://www.linkedin.com/in/thomas-krapf-05b41a/'
headless: true
---

### Thomas
### Krapf

**Co-Founder & CTO**

Two decades of re-/insurance business development experience. Has held senior roles globally, including in New York, London, Paris, Munich and Zurich. Helped build 3 startups and electronic re/-insurance initiatives such as Rueschlikon, ACORD, eBOT, and eCOT.