---
title: "Peter Belko"
date: 2021-01-03T17:57:24+01:00
draft: false
social:
 linkedin:
  url: 'https://www.linkedin.com/in/peterbelko/'
headless: true
---

### Peter
### Belko

**Platform Lead**

13 Years in technology where he worked as Data Engineer and Head of Engineering for multiple startups in London and Bratislava delivering mass-data solutions for the telco and financial services sectors. Experience architecting and developing big data solutions with a focus on stream processing, scalability and high availability.