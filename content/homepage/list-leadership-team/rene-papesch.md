---
title: "Rene Papesch"
date: 2021-01-04T17:57:24+01:00
draft: false
social:
 linkedin:
  url: 'https://www.linkedin.com/in/rene-papesch'
headless: true
---

### René
### Papesch

**Co-Founder & CTO**

12 years of business and technology consulting. 8 years of experience in large-scale data processing projects. Bridging the gap between tech, data and processes, with the last 5 years in the re-insurance industry. Experience working in Vienna, Zurich, Frankfurt and Bratislava and founded two other companies.