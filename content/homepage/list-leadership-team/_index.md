---
title: "Leadership Team with 70+ Years Experience"
date: 2021-01-24T17:57:24+01:00
draft: false
headless: true
resources:
- name: Featured
  src: '*.{png,jpg}'
  title: Team Members
---

# Leadership Team with 70+ Years Experience

Riskwolf's team combines deep reinsurance industry experience with strong technical skills in large-scale data processing. Our team has 70+ years experience in leading re-/insurance companies and financial services in Europe and Asia.
