---
title: "Milos Molnar"
date: 2021-01-02T17:57:24+01:00
draft: false
social:
 linkedin:
  url: 'https://www.linkedin.com/in/milosmolnar/'
headless: true
---

### Milos
### Molnar

**Big Data Architect**

Milos has 25 years of experience in the technology field, focused on distributed systems, data processing and management (Apache Hadoop and Kafka) and develops real-time streaming solutions for telcos and financial services companies. He is also the founder of trivia data in Bratislava.