---
title: "We’re facing a seismic shift towards digitalization"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
resources:
- name: Featured
  src: 'img/**.{png,jpg}'
  title: 'Sesmic shift'
---

Globally, we are facing a seismic shift. The core fabric of the global economy - the way we work, commute, travel and consume - is transforming right before our eyes.

Digitization means: Consumers, businesses, processes, devices and data are all interconnected. As a result of this ongoing transformation, new risks are emerging:

For instance, increasing internet utilization leads to stress to its backbone causing more frequent outages.

Today only a fraction of these new risks are insurable. Reasons for this are __contract uncertainty__, difficulty to __scale the claims process__, and __internal legacy__. There is a **multi-billion** missed growth opportunity for the insurance industry.