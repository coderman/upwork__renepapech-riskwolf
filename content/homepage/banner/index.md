---
title: "Banner Homepage"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
cta:
  title: Learn more...
  url: '#platform'
resources:
- name: Featured
  src: 'img/**.{png,jpg}'
- name: Slider
  src: 'slider/**.{png,jpg,svg}'
  title: 'Slider Logo'
---

# Turn Real-Time Data Into Insurance

#### Riskwolf enables insurers to build and operate parametric coverages for digital tasks.
