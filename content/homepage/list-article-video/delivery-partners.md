---
title: "Digital Business Interruption for Delivery Partners"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
video: 'https://player.vimeo.com/video/406195297'
location: India
---

{{< copy/medium >}}
Riskwolf is also creating a multiple-peril product to protect food delivery drivers in India. This product covers Internet connectivity outages, lost revenue due to weather conditions, and service cancellations.
{{< /copy/medium >}}

