---
title: "innovate"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
icon: conversation
---

# INNOVATE

Shorter product innovation cycles - idea-to-market within weeks
