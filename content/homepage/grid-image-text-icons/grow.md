---
title: "grow"
date: 2021-01-25T17:57:24+01:00
draft: false
headless: true
icon: stats
---

# GROW

Top-line growth due to the coverage of emerging digital risks
