---
title: 'Regional Market Lead Asia'
date: 2021-01-27T17:57:24+01:00
draft: false
subtitle: 'Asia / Full-time'
---

As a regional market lead you work in a remote-first team, are representing Riskwolf in Asia and understand the clients needs and drive the market entry and penetration.

### Your tasks

- Market representation in Asia for Riskwolf "Regional Market Lead"
- Establish client relationships and sales
- Understanding the client's needs
- Help shape the product and capture requirements
- Coordinate client implementations
- Continiously improve quality and strive for customer retention

## Our offering and values

DEFINE THAT

## Your skills and experiences

- Strong communication skills in English and optimal a local language
- Understand data and technical limitations
- Able to work remote-first - Hands-on and no fear of new technics and tools
- Self-starter - proactivly take opportunities and address problems
Market insights in ASEAN and existing network in telco, food delivery or any other digital - vertical
- See the glass half-full and not half-empty
- Preferred locations: Malaysia, Indonesia, Thailand, Vietnam
- MSc degree preferred in business and/or technology
