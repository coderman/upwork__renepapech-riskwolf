---
title: "remote first"
date: 2021-01-27T17:57:24+01:00
draft: false
headless: true
icon: conversation
---

# REMOTE-FIRST

We're a completely distributed team, so you can work where it makes you happy.
