---
title: 'Data Scientist'
date: 2021-01-26T17:57:24+01:00
draft: false
subtitle: 'Any location / Full-time'
---

We are growing and our strategic interest is to build up a strong data team that scouts new sources for connectivity risks and turn them into priceable insurance products. You will work closely with the management team and delivery teams to build next generation insurance products that are purley based on data. Your daily work will be working with innovative and new sources of data for Internet quality and availability information.

### Your tasks


- Identify new data sources that can be used for pricing and better understanding connectivity risks
- Understand and challange data and methods from 3rd parties
- Data preparation and cleansing
- Design and simulate parametric insurance products based on clear inputs
- Model fitting and tuning incl. ML techniques
- Deployment of pricing algorithmn on the Riskwolf platform
- Lead the route for building a strong data team


## Our offering and values

DEFINE THAT

## Your skills and experiences

- You are self-motivated and a manager of one
- You want to take the lead on building up the data team at Riskwolf
- You can communicate complex reports in a simple to understand language
- You can work with large-scale data
- You have undergone actuary training and an understanding of the insurance domain (>2 years)
- You have the skills to apply predictive analytics and statistical modelling in R, Python and/or - Matlab
- You know machine learning models
