---
title: "Footer Main"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
background:
  url: 'assets/images/footer-vector-img-2.png'
---

_The platform that insures the digital economy._

Riskwolf’s vision is to make digital enterprises more resilient.

We help visionary re-insurers globally to create protection for digital risks that are underserved by traditional insurance.