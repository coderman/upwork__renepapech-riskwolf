
# Riskwolf AG Hugo Theme


## Requirements
* Hugo, v0.73+

## General information
This repository contains the entire web-site including the `Hugo` site structure, content files and the theme files.

## Instructions
1. Copy the repository to your local machine
2. In the root of the project,  run the following terminal commands:
    * `hugo serve -D` - to spin up a virtual server for local preview of the site (http://localhost:1313)
    * `hugo` - to generate site's static files. New directory will be created called `public`. These files are used to upload your site to the server.

## Adding new content:
New content can be added by either:
* manually creating Markdown files
* by using Hugo's build in content creator - recommended for types of content like job posts or new leadership member. By using this option, post's creation time is automatically added as well as some basic required data.

### Adding by content types
New job post (`job`) - default
```
hugo new job/{file_name}.md
```

New member of the leadership team (`homepage/list-leadership-team`)
```
hugo new --kind list-leaderhip-team homepage/list-leaderhip-team/{file_name}.md
```

## Content structure
Listing pages, like "Jobs" page for example, have two types of content files:
1. listing page content file - `_index.md`
2. post content file - `{post_name}.md`

Listing page content file contains all the data that is relevant to that listing page. Post content is listed automatically and it comes from the individual post's files.

## Featured images
Some page sections contain a so-called  _"featured"_ image. This is an image file that will automatically be loaded into the corresponding page section without the need for the content editor to specify them explicitly. Example of these kind of elements are "hero banner", grey colored "image-text" and the "platform" page sections. These images are to be placed within the directory called `img`. Supported formats are _jpg_ and _png_.

**Note: due to technical restrictions, only _one_ image is to be placed within the mentioned `img` directory!**

## Logo sliders
In order to add/remove items in the logo slider, simply add/remove the image(s) located in the `content/homepage/banner/slider` directory.

## Site settings
All global site settings are defined in he `config.yml` file which is residing in the root directory. These include: page title, site URL, static image assets like logos, footer and header menus.
